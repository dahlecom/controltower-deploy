Controltower-deploy
===================

This repository contains kubernetes manifests for deploying controltower using kustomize

base/ contains the base manifests

overlays/ contains the customizations for each environment

To generate valid manifests for a target environment, run:
`kustomize build overlays/<environment>`

The outut can be fed to kubectl for applying to kubernetes, like:
`kustomize build overlays/test | kubectl apply -f -`

The ingress resources reference a secret called tls-dahl-se for tls cert. The secret manifest is not part of this repo for security reasons.
