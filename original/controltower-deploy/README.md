# Docker-compose for Control Tower

## Quick start

There are three different parts to deploy de Control Tower project using ``docker-compose``:

- **traefik**, used as external entry point to split the requests and handle the SSL certificate.
- **backend**, with the API, WebSockets server and database.
- **frontend**, with the frontend application.

To run Traefik:

    $ cd traefik/
    $ docker-compose up

To run the backend:

    $ cd backend/
    $ docker-compose up

To run the frontend:

    $ cd frontend/
    $ docker-compose up


## Entry points

- Frontend: https://tst.controltower.dahl.se
- Backend base URL: https://backend.tst.controltower.dahl.se
- Admin panel: https://backend.tst.controltower.dahl.se/admin/

You can change the environment variable ``CONTROLTOWER_DOMAIN`` in the ``.env`` files 
for backend and frontend to use a different domain.

## Superuser

Once the service is running, you can create a superuser to access the admin panel using the following command in the ``backend`` folder:

    $ docker-compose run --rm backend ./manage.py createsuperuser